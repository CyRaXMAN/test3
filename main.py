#!/usr/bin/python3

source = [
    (None, 'a'),
    (None, 'b'),
    (None, 'c'),
    ('a', 'a1'),
    ('a', 'a2'),
    ('a2', 'a21'),
    ('a2', 'a22'),
    ('b', 'b1'),
    ('b1', 'b11'),
    ('b11', 'b111'),
    ('b', 'b2'),
    ('c', 'c1'),
]

expected = {
    'a': {'a1': {}, 'a2': {'a21': {}, 'a22': {}}},
    'b': {'b1': {'b11': {'b111': {}}}, 'b2': {}},
    'c': {'c1': {}},
}


def to_tree(source_list):
    """
    Generate tree from list of tuples with pair elements

    :param source_list: list of tuples
    :return: tree structure in python dictionary
    :rtype: dict
    """
    def _search_key(target_dict, haystack):
        """
        Search parent dict by key

        :param target_dict: dict to search
        :param haystack: dict key
        :return: required dictionary
        :rtype: dict
        """
        if haystack in target_dict:
            return target_dict
        for k, v in target_dict.items():
            if isinstance(v, dict):
                required_dict = _search_key(v, haystack)
                if required_dict is not None:
                    return required_dict

    tree = {}
    for element in source_list:
        parent, value = element
        if parent is None:
            tree[value] = dict()  # top elements
        else:
            parent_dict = _search_key(tree, parent)
            parent_dict[parent].setdefault(value, {})

    return tree


assert to_tree(source) == expected
